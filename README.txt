URLify module readme
--------------------

URLify is a simple module that generates the path alias of a node automatically
using javascript. The alias is generated from the title of the node.

It is based on the SlugField from the Django project.

Please note that URLify is not meant to be a replacement for the pathauto
module, it just saves you some typing. It has been tested on Firefox,
Konqueror and IE6.


Requirements
------------

-- Drupal 4.7
-- path module
-- a title field in the node type

Installation
------------

1. Extract the urlify module to your modules directory.
2. Enable the urlify module on the administer >> modules page. 
  (No database change is neccessary)
3. Modify settings at administer >> settings >> urlify


Author
------

Nesta Campbell <nesta.campbell@gmail.com>


TODO
----

 * Allow node types to define which field the alias should be generated from
 * Check for duplicates (maybe not)

References and Credits
----------------------

Original javascript and idea from the Django project.
http://www.djangoproject.com/
http://code.djangoproject.com/browser/django/trunk/django/contrib/admin/media/js/urlify.js
